var gl;
var colorUniformLocation;
var ctmUniformLocation;
var mProjectionUniformLocation;
var mViewUniformLocation;
var instances = [];
var instancesNum = 0;
var sliderTx;
var sliderTy;
var sliderR;
var sliderSx;
var sliderXy;
var sliderAxnAlfa;
var sliderAxnBeta;
var sliderOblAlfa;
var sliderOblBeta;
var sliderPerD;
var program;
var allShapes;
var chosenShape = "0";
var drawModeWF = 0;
var at = [0, 0, 0];
var eye = [1, 1, 1];
var up = [0, 1, 0];
var alfaAxn = 0.0;
var betaAxn = 0.0;
var radiusAxn = 0.0;
var alfaObl = 0.0;
var betaObl = 0.0;
var radiusObl = 0.0;
var perD = 1.0;
var x = 0.0;
var y = 0.0;
var z = 0.0;
var zoomScale = 1;

const projectionType = Object.freeze({ //"name" parameter corresponds to name used in http file when calling update func
    "orhtoFront": { value: 1, name: "Front", type: "Orthogonal" },
    "orhtoSide": { value: 2, name: "Right", type: "Orthogonal" },
    "orhtoFloor": { value: 3, name: "Floor", type: "Orthogonal" },
    "isometric": { value: 4, name: "Isometric", type: "Axonometric" },
    "dimetric": { value: 5, name: "Dimetric", type: "Axonometric" },
    "trimetric": { value: 6, name: "Trimetric", type: "Axonometric" },
    "axonometricFree": { value: 7, name: "FreeAx", type: "Axonometric" },
    "cavalier": { value: 8, name: "Cavalier", type: "Oblique" },
    "cabinet": { value: 9, name: "Cabinet", type: "Oblique" },
    "obliqueFree": { value: 10, name: "FreeOb", type: "Oblique" },
    "perspectiveProj": { value: 11, name: "perspectiveProj", type: "Perspective" }
});
var viewsHistory = [projectionType.orhtoFront, projectionType.isometric, projectionType.cavalier, projectionType.perspectiveProj];

function updateCTM(name = null) {
    instances[instancesNum - 1] = mult(translate((sliderTx.value / 50) - 1, (sliderTy.value / 50) - 1, 0), rotateZ(sliderR.value));
    if(name == "FreeAx"){
        eye = calculateXYZ(alfaAxn, betaAxn);
    }
    else if(name == "FreeOb"){
        eye = calculateXYZ(alfaObl, betaObl);
    }    
}

function keyDownHandler(event) {
    if (event.keyCode == 87) { //W
        drawModeWF = 0;
    } else if (event.keyCode == 70) { //F
        drawModeWF = 1;
    } else if (event.keyCode == 90) { //Z
        console.log("Z buffer: ");
        if (gl.isEnabled(gl.DEPTH_TEST) == false) {
            gl.enable(gl.DEPTH_TEST);
        } else {
            gl.disable(gl.DEPTH_TEST);
        }
        console.log(gl.isEnabled(gl.DEPTH_TEST));
    } else if (event.keyCode == 66) { //B
        console.log("B cull face: ");
        if (gl.isEnabled(gl.CULL_FACE) == false) {
            // gl.FRONT - The positively oriented faces on the screen
            // gl.BACK - The negatively oriented faces on the screen
            // gl.FRONT_AND_BACK - Both sides (with any orientation)

            gl.enable(gl.CULL_FACE);
            gl.frontFace(gl.FRONT)
        } else {
            gl.disable(gl.CULL_FACE);
        }
        console.log(gl.isEnabled(gl.CULL_FACE));
    }
}

function openTab(evt, tabName) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";

    result = viewsHistory.find(function(element){
        return element.type == tabName;}).name;
    changeView(evt, result);
}

function hideSliders(){
    document.getElementById("slidersAxn").style.display = "none";
    document.getElementById("slidersObl").style.display = "none";
}

function changeView(evt, viewName) {
    hideSliders();
    at = [0,0,0];
    switch (viewName) {
        case projectionType.orhtoFront.name: //Projection types specified in the beginning of the file
            eye = calculateXYZ(0,90);
            viewsHistory[0] = projectionType.orhtoFront;
            break;
        case projectionType.orhtoSide.name:
            eye = calculateXYZ(0,0); 
            viewsHistory[0] = projectionType.orhtoSide;
            break;
        case projectionType.orhtoFloor.name:
            eye = calculateXYZ(90,90); 
            viewsHistory[0] = projectionType.orhtoFloor;
            break;
        case projectionType.isometric.name:
            eye = [1,1,1]
            viewsHistory[1] = projectionType.isometric;
            break;
        case projectionType.dimetric.name:
            eye = calculateXYZ(42,7); 
            viewsHistory[1] = projectionType.dimetric;
            break;
        case projectionType.trimetric.name:
            eye = calculateXYZ(54.16, 23.16); 
            viewsHistory[1] = projectionType.trimetric;
            break;
        case projectionType.axonometricFree.name:
            document.getElementById("slidersAxn").style.display = "inline";
            viewsHistory[1] = projectionType.axonometricFree;
            updateCTM(projectionType.axonometricFree.name);
            break;
        case projectionType.cavalier.name:
            eye = calculateXYZ(34,45)
            viewsHistory[2] = projectionType.cavalier;
            break;
        case projectionType.cabinet.name:
            eye = calculateXYZ(29,66)
            viewsHistory[2] = projectionType.cabinet;
            break;
        case projectionType.obliqueFree.name:
            document.getElementById("slidersObl").style.display = "inline";
            updateCTM(projectionType.obliqueFree.name);
            viewsHistory[2] = projectionType.obliqueFree;
            break;
        case projectionType.perspectiveProj.name:
            at = [0,0,perD]
            break;
        default:
            break;
    }
}

function resize(canvas) {
    var displayWidth = canvas.clientWidth;
    var displayHeight = canvas.clientHeight;

    // Check if the canvas is not the same size.
    if (canvas.width != displayWidth ||
        canvas.height != displayHeight) {

        // Make the canvas the same size
        canvas.width = displayWidth;
        canvas.height = displayHeight;
    }
}

function calculateXYZ(alfa, beta) {
    x = Math.cos(alfa*Math.PI/180)*Math.cos(beta*Math.PI/180)
    y = Math.sin(alfa*Math.PI/180)
    z = Math.cos(alfa*Math.PI/180)*Math.sin(beta*Math.PI/180)
    return [x,y,z]
}

window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) { alert("WebGL isn't available"); }

    //Sliders
    sliderTx = document.getElementById("sliderTx");
    sliderTy = document.getElementById("sliderTy");
    sliderR = document.getElementById("sliderR");
    sliderSx = document.getElementById("sliderSx");
    sliderSy = document.getElementById("sliderSy");
    allShapes = document.getElementsByName("chosenObject");
    sliderAxnAlfa = document.getElementById("sliderAxnAlfa");
    sliderAxnBeta = document.getElementById("sliderAxnBeta");
    sliderOblAlfa = document.getElementById("sliderOblAlfa");
    sliderOblBeta = document.getElementById("sliderOblBeta");
    sliderPerD = document.getElementById("sliderPerD")

    document.addEventListener('keydown', keyDownHandler, false);
    document.getElementById('Orthogonal').click();

    document.addEventListener('wheel', function(event){
        if(zoomScale - (event.deltaY * 0.005) > 0.5)
            zoomScale = zoomScale - (event.deltaY * 0.005);
    }, false);

    sliderTx.oninput = function() {
        updateCTM();
    }
    sliderTy.oninput = function() {
        updateCTM();
    }
    sliderR.oninput = function() {
        updateCTM();
    }
    sliderSx.oninput = function() {
        updateCTM();
    }
    sliderSy.oninput = function() {
        updateCTM();
    }
    sliderAxnAlfa.oninput = function() {
        alfaAxn = sliderAxnAlfa.value
        updateCTM(projectionType.axonometricFree.name);
    }
    sliderAxnBeta.oninput = function() {
        betaAxn = sliderAxnBeta.value
        updateCTM(projectionType.axonometricFree.name);
    }
    sliderOblAlfa.oninput = function() {
        alfaObl = sliderOblAlfa.value
        updateCTM(projectionType.obliqueFree.name);
    }
    sliderOblBeta.oninput = function() {
        betaObl = sliderOblBeta.value;
        updateCTM(projectionType.obliqueFree.name);
    }
    sliderPerD.oninput = function(){
        perD = (sliderPerD.value/50)-1;
        at = [0,0,perD];
    }
    
    for (var i = 0; i < allShapes.length; i++) {
        allShapes[i].onclick = function() {
            chosenShape = this.value;
        }
    }
    for (var i = 0; i < document.getElementsByName("drawingMode").length; i++) {
        document.getElementsByName("drawingMode")[i].onclick = function() {
            drawModeWF = this.value;
        }
    }

    // Three vertices
    var vertices = [
        vec2(-0.75, -0.75),
        vec2(0, 0.75),
        vec2(0.75, -0.75),
    ];

    // Configure WebGL
    gl.viewport(0, 0, canvas.width, canvas.height);
    gl.clearColor(0.3, 0.3, 0.3, 1.0);

    // Load shaders and initialize attribute buffers
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    cubeInit(gl);
    sphereInit(gl);
    cylinderInit(gl);
    torusInit(gl);
    bunnyInit(gl);

    // Load the data into the GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    // Associate our shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    colorUniformLocation = gl.getUniformLocation(program, "vColor");
    ctmUniformLocation = gl.getUniformLocation(program, "ctm");
    mViewUniformLocation = gl.getUniformLocation(program, "mView");
    mProjectionUniformLocation = gl.getUniformLocation(program, "mProjection");

    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);

    this.instances.push(new mat4());
    this.instancesNum++;

    render();
}

function render() {
    resize(gl.canvas);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    var aspectRatio = gl.canvas.width / gl.canvas.height;
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    var mView = lookAt(eye, at, up);
    var mProjection = ortho(-1 * aspectRatio * zoomScale, 1 * aspectRatio* zoomScale, -1* zoomScale, 1* zoomScale, 10, -10);

    gl.uniformMatrix4fv(mViewUniformLocation, false, flatten(mView));
    gl.uniformMatrix4fv(mProjectionUniformLocation, false, flatten(mProjection));
    gl.uniformMatrix4fv(ctmUniformLocation, false, flatten(instances[instancesNum - 1]));

    if (drawModeWF == 0) { //Draw just edges or whole figures
        switch (chosenShape) { //What shape to draw
            case "0":
                cubeDrawWireFrame(gl, program);
                break;
            case "1":
                sphereDrawWireFrame(gl, program);
                break;
            case "2":
                cylinderDrawWireFrame(gl, program);
                break;
            case "3":
                torusDrawWireFrame(gl, program);
                break;
            case "4":
                bunnyDrawWireFrame(gl, program);
                break;
            default:
                break;
        }
    } else {
        switch (chosenShape) {
            case "0":
                cubeDrawFilled(gl, program);
                break;
            case "1":
                sphereDrawFilled(gl, program);
                break;
            case "2":
                cylinderDrawFilled(gl, program);
                break;
            case "3":
                torusDrawFilled(gl, program);
                break;
            case "4":
                bunnyDrawFilled(gl, program);
                break;
            default:
                break;
        }
    }
    requestAnimFrame(render);
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}